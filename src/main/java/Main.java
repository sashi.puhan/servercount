
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;

public class Main {

    static Properties prop;
    static ArrayList<String> prodvolume = new ArrayList<>();
    static ArrayList<Object> prodtime = new ArrayList<>();

    static ArrayList<String> qavolume = new ArrayList<>();
    static ArrayList<Object> qatime = new ArrayList<>();
    private static final String WEBHOOK = "https://aliplb.webhook.office.com/webhookb2/03b8a2e0-6187-4681-9864-b7bd57a2e71f@63d27bed-9d4d-4820-ad98-7fe5353ead34/IncomingWebhook/0e4de69f710a49f596117d8294a7cf3c/e561f107-a386-48b9-86f8-52c2c990c748";

    public Main() {
        try {
            prop = new Properties();
            FileInputStream ip = new FileInputStream("src/main/resources/config.properties");
            prop.load(ip);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void SendData() {
        try {
            StringBuilder sb1 = new StringBuilder(200);
            sb1.append("<table width =\"300\" table border=\"1\"> ");
            sb1.append("<caption><b>Prod and QA Server Volume Count</b></caption>\n");
            sb1.append("<tr><th>Tables</th><th>ProdVolume</th><th>ProdTime</th><th>QAVolume</th><th>QATime</th></tr>\n");
            sb1.append("<tr><td>DimClientNotifications</td><td>" + prodvolume.get(0) + "</td><td>" + prodtime.get(0) + "</td><td>" + qavolume.get(0) + "</td><td>" + qatime.get(0) + "</td></tr>\n");
            sb1.append("<tr><td>Fact_Binder</td><td>" + prodvolume.get(1) + "</td><td>" + prodtime.get(1) + "</td><td>" + qavolume.get(1) + "</td><td>" + qatime.get(1) + "</td></tr>\n");
            sb1.append("<tr><td>Fact_OCR</td><td>" + prodvolume.get(2) + "</td><td>" + prodtime.get(2) + "</td><td>" + qavolume.get(2) + "</td><td>" + qatime.get(2) + "</td></tr>\n");
            sb1.append("<tr><td>Insight_Debt</td><td>" + prodvolume.get(3) + "</td><td>" + prodtime.get(3) + "</td><td>" + qavolume.get(3) + "</td><td>" + qatime.get(3) + "</td></tr>\n");
            sb1.append("<tr><td>Insight_Income</td><td>" + prodvolume.get(4) + "</td><td>" + prodtime.get(4) + "</td><td>" + qavolume.get(4) + "</td><td>" + qatime.get(4) + "</td></tr>\n");
            sb1.append("<tr><td>Insight_Investment</td><td>" + prodvolume.get(5) + "</td><td>" + prodtime.get(5) + "</td><td>" + qavolume.get(5) + "</td><td>" + qatime.get(5) + "</td></tr>\n");
            sb1.append("<tr><td>Insight_Others</td><td>" + prodvolume.get(6) + "</td><td>" + prodtime.get(6) + "</td><td>" + qavolume.get(6) + "</td><td>" + qatime.get(6) + "</td></tr>\n");
            sb1.append("<tr><td>Insight_Property</td><td>" + prodvolume.get(7) + "</td><td>" + prodtime.get(7) + "</td><td>" + qavolume.get(7) + "</td><td>" + qatime.get(7) + "</td></tr>\n");
            sb1.append("<tr><td>Insights_Detail</td><td>" + prodvolume.get(8) + "</td><td>" + prodtime.get(8) + "</td><td>" + qavolume.get(8) + "</td><td>" + qatime.get(8) + "</td></tr>\n");
            sb1.append("<tr><td>STG_FIUser</td><td>" + prodvolume.get(9) + "</td><td>" + prodtime.get(9) + "</td><td>" + qavolume.get(9) + "</td><td>" + qatime.get(9) + "</td></tr>\n");
            sb1.append("<tr><td>STG_TaxFilerDocument</td><td>" + prodvolume.get(10) + "</td><td>" + prodtime.get(10) + "</td><td>" + qavolume.get(10) + "</td><td>" + qatime.get(10) + "</td></tr>\n");
            sb1.append("<tr><td>STG_TaxFilerUser</td><td>" + prodvolume.get(11) + "</td><td>" + prodtime.get(11) + "</td><td>" + qavolume.get(11) + "</td><td>" + qatime.get(11) + "</td></tr>\n");
            String in = sb1.toString();
            sb1.append("</table>");
            System.out.printf(sb1.toString());

//            JsonObject itemdata = new JsonObject();
//            itemdata.addProperty("text", String.valueOf(sb1));
//            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//            HttpPost post = new HttpPost(WEBHOOK);
//            StringEntity postingString = new StringEntity(itemdata.toString());
//            //  post.setHeader("Authorization", "Basic " + authEncoded);
//            post.setEntity(postingString);
//            post.setHeader("Content-type", "application/json");
//            HttpResponse response = (HttpResponse) httpClient.execute(post);
//            System.out.println("*************Now Received response Server***********");
//            System.out.println(response);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }


    public static void runBotAndPublishResult() {

        Main test = new Main();

        String connectionUrlProd =
                "jdbc:sqlserver://prddataanalyticssynapsews.sql.azuresynapse.net;"
                        + "database=HalcyonDWH;"
                        + "user=sqladminuser;"
                        + "password=H@lcy0n@@D3H!2022$$**;"
                        + "encrypt=true;"
                        + "trustServerCertificate=false;"
                        + "loginTimeout=30;";

        String connectionUrlQA =
                "jdbc:sqlserver://halcyon-dataanalytics-qasql.database.windows.net;"
                        + "database=halcyon-qa;"
                        + "user=qahalcyonadmin;"
                        + "password=Welcome@12345;"
                        + "encrypt=true;"
                        + "trustServerCertificate=false;"
                        + "loginTimeout=30;";

        ResultSet resultSet = null;

        try (Connection connection = DriverManager.getConnection(connectionUrlProd);
             Statement statement = connection.createStatement();) {
            for (int i = 1; i <= prop.size(); i++) {
                String selectSql = prop.getProperty(String.valueOf(i));
                long startTime = System.nanoTime();
                resultSet = statement.executeQuery(selectSql);
                while (resultSet.next()) {
                    prodvolume.add(resultSet.getString(1));
                     System.out.println(resultSet.getString(1));
                }
                long endTime = System.nanoTime();
                double value = (endTime - startTime) / 1e6;
                double val = (1000 / value) / 10;
                String sss = String.format("%.01f", val);

                prodtime.add(sss);
                //  System.out.println(1000 / value);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try (Connection connection = DriverManager.getConnection(connectionUrlQA);
             Statement statement = connection.createStatement();) {
            for (int i = 1; i <= prop.size(); i++) {
                String selectSql = prop.getProperty(String.valueOf(i));
                long startTime = System.nanoTime();
                resultSet = statement.executeQuery(selectSql);
                while (resultSet.next()) {
                    qavolume.add(resultSet.getString(1));
                  //   System.out.println(resultSet.getString(1));
                }
                long endTime = System.nanoTime();
                double value = (endTime - startTime) / 1e6;
                double val = (1000 / value) / 10;
                String sss = String.format("%.01f", val);

                qatime.add(sss);
                //  System.out.println(1000 / value);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        test.SendData();
        prodvolume.clear();
        prodtime.clear();
        qavolume.clear();
        qatime.clear();
    }


    public static void main(String[] args) {

        runBotAndPublishResult();

    }
}
